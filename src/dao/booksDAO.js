const knex = require('knex');
require('dotenv').config();

class BooksDAO {
  constructor () {
    // eslint-disable-next-line no-unused-expressions
    this.db = knex({
      client: 'mysql',
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
      },
    // eslint-disable-next-line no-sequences
    }),
    this.table = 'books'
  }

  async find (id) {
    return this.db
      .select('*')
      .from(this.table)
      .where('id', id);
  }

  async update (book, id) {
    return this.db(this.table)
      .update(book)
      .where('id', id);
  }

  async delete (id) {
    return this.db
      .del()
      .from(this.table)
      .where('id', id);
  }

  async create (book) {
    return this.db(this.table).insert(book);
  }

  async findAll (id) {
    return this.db
      .select('*')
      .from(this.table)
  }
}

module.exports = new BooksDAO();
