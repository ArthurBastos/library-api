const bookDAO = require('../dao/booksDAO');

class BookController {
  async find (req, res) {
    try {
      const { id } = req.params;
      const book = await bookDAO.find(id);
      return res.status(200).json(book[0]);
    } catch (e) {
      console.log(e);
      return res.status(500).json({ error: e });
    }
  }

  async findAll (req, res) {
    try {
      const books = await bookDAO.findAll();
      return res.status(200).json(books);
    } catch (e) {
      console.log(e);
      return res.status(500).json({ error: e });
    }
  }

  async create (req, res) {
    try {
      await bookDAO.create(req.body);
      return res.status(201).json();
    } catch (e) {
      console.log(e);
      return res.status(500).json({ error: e });
    }
  }

  async update (req, res) {
    try {
      const { id } = req.params;
      await bookDAO.update(req.body, id);
      return res.status(204).json();
    } catch (e) {
      console.log(e);
      return res.status(500).json({ error: e });
    }
  }

  async delete (req, res) {
    try {
      const { id } = req.params;
      await bookDAO.delete(id);
      return res.status(204).json();
    } catch (e) {
      console.log(e);
      return res.status(500).json({ error: e });
    }
  }
}

module.exports = new BookController();
