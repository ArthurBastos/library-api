const { Router } = require('express');
const booksCtrl = require('./books.controller')

const router = new Router()
router.route('/books/:id').get(booksCtrl.find)
router.route('/books').get(booksCtrl.findAll)
router.route('/books').post(booksCtrl.create)
router.route('/books/:id').put(booksCtrl.update)
router.route('/books/:id').delete(booksCtrl.delete)

module.exports = router;
