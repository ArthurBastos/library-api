const express = require('express')
const path = require('path')
const cors = require('cors')
const books = require('./src/api/books.router');

const PORT = process.env.PORT || 5000
const app = express();
app.use(express.json());
app.use(cors());
app.use(books);

app.get('/', (req, res) => {
  res.send('ok');
})
app.use('/book', books);
app.listen(PORT, () => {
  console.log(`app listening at ${PORT}`);
})
